import React, { useState } from 'react';
import ListOfGifs from '../components/ListOfGifs';

export default function App() {
  document.title = 'Giphy App';
  const [keyword, setKeyword] = useState('');

  const handleSubmmit = (evt) => {
    evt.preventDefault();
    const inputKeyword = evt.target.previousElementSibling.value;
    setKeyword(inputKeyword);
  };

  return (
    <div className='App'>
      <section className='App-content'>
        <form action=''>
          <input type='text' id='keyword' />
          <button onClick={handleSubmmit} type='submit'>BUSCAR</button>
        </form>
        <ListOfGifs keyword={keyword} />
      </section>
    </div>
  );
}
