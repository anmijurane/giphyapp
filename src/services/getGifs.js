
const APIKEY = 'QMoUAXKGyw5kDpAGbiUqkS096rZidIor';
const rating = 'pg';
const offset = 0;

export default function getGifs(
  { keyword = 'Rick', limit = 10, languaje = 'en' } = {},
) {

  const api = `https://api.giphy.com/v1/gifs/search?api_key=${APIKEY}&q=${keyword}&limit=${limit}&offset=${offset}&rating=${rating}&lang=${languaje}`;

  return fetch(api)
    .then((res) => res.json())
    .then((response) => {
      const { data = [] } = response;
      if (Array.isArray(data)) {
        const gift = data.map((image) => {
          const { images, title, id } = image;
          const { url } = images.downsized_medium;
          return { title, id, url };
        });
        return gift;
      }
    });
}
