import React from 'react';

export default function Gif({ url, title = 'nothingTitle' }) {
  return (
    <div className='image--container'>
      <h4>{title}</h4>
      <img src={url} alt={title} />
    </div>
  );
}
