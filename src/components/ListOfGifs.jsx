import React, { useEffect, useState } from 'react';
import getGifs from '../services/getGifs';
import Gif from './Gif';

export default function ListOfGifs({ keyword }) {

  const [gifs, setGifs] = useState([]);

  useEffect(() => {
    getGifs({ keyword, limit: 5 })
      .then((gifsAll) => setGifs(gifsAll));
  }, [keyword]);

  return gifs.map(
    (gf) => (
      <Gif
        key={gf.id}
        title={gf.title}
        url={gf.url}
      />
    ),
  );
}
