import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/__main__.scss';

import App from './pages/App';

ReactDOM.render(<App />, document.getElementById('app'));
